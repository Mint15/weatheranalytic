import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class WeatherAnalytic {

    public static void main(String[] args) {

        System.out.println("Пожалуйста, введите абсолютный путь к файлу:");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        File file = new File(s);
        WeatherAnalytic weatherAnalytic = new WeatherAnalytic();

        weatherAnalytic.analyze(file);
    }

    File analyze(File file) {
        File result = new File(file.getParent() + "\\result.txt");

        ArrayList<Weather> weathers = new ArrayList<>();

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            for (int i = 0; i < 10; i++) {
                bufferedReader.readLine();
            }

            String currentLine;

            while ((currentLine = bufferedReader.readLine()) != null){
                Integer year = Integer.valueOf(currentLine.substring(0, 4));
                String month = currentLine.substring(4, 6);
                String day = currentLine.substring(6, 8);
                String time = currentLine.substring(9, 11) + ":" + currentLine.substring(11, 13);
                currentLine = currentLine.substring(14);
                Double temperature = Double.valueOf(currentLine.substring(0, currentLine.indexOf(',')));
                currentLine = currentLine.substring(currentLine.indexOf(',') + 1);
                Double relativeHumidity = Double.valueOf(currentLine.substring(0, currentLine.indexOf(',')));
                currentLine = currentLine.substring(currentLine.indexOf(',') + 1);
                Double windSpeed = Double.valueOf(currentLine.substring(0, currentLine.indexOf(',')));
                currentLine = currentLine.substring(currentLine.indexOf(',') + 1);
                Double windDirection = Double.valueOf(currentLine.substring(0));

                Weather weather = new Weather(year, month, day, time, temperature,
                        relativeHumidity, windSpeed, windDirection);
                weathers.add(weather);
            }

            bufferedReader.close();
            fileReader.close();

            FileWriter fileWriter = new FileWriter(result);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write("Средняя температура: " + getAverageTemperature(weathers) + "\n");
            bufferedWriter.write("Средняя влажность: " + getAverageHumidity(weathers) + "\n");
            bufferedWriter.write("Средняя скорость ветра: " + getAverageWindSpeed(weathers) + "\n");


            bufferedWriter.write("Самая высокая температура была " + getHighestTemperature(weathers) + "\n");
            bufferedWriter.write("Самая низкая влажность была " + getLowestHumidity(weathers) + "\n");
            bufferedWriter.write("Самый сильный ветер был " + getStrongestWind(weathers) + "\n");

            bufferedWriter.write("Самое частое направление ветра: " + getMostFrequentWindDirection(weathers));

            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private String getMostFrequentWindDirection(ArrayList<Weather> weathers) {
        int[] direction = new int[4];
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getWindDirection() < 45d || weathers.get(i).getWindDirection() >= 315d){
                direction[0] = direction[0] + 1;
            }
            if (weathers.get(i).getWindDirection() >= 225d && weathers.get(i).getWindDirection() < 315d){
                direction[1] = direction[1] + 1;
            }
            if (weathers.get(i).getWindDirection() >= 135d && weathers.get(i).getWindDirection() < 225d){
                direction[2] = direction[2] + 1;
            }
            if (weathers.get(i).getWindDirection() >= 45d && weathers.get(i).getWindDirection() < 135d){
                direction[3] = direction[3] + 1;
            }
        }
        int max = 0;
        int index = 0;
        for (int i = 0; i < direction.length; i++) {
            if (direction[i] > max){
                max = direction[i];
                index = i;
            }
        }
        String s = "";
        switch (index){
            case (0):
                s = "север";
                break;
            case (1):
                s = "запад";
                break;
            case (2):
                s = "юг";
                break;
            case (3):
                s = "восток";
                break;
            default:
                break;
        }
        return s;
    }

    private Double getAverageWindSpeed(ArrayList<Weather> weathers) {
        Double sum = 0d;
        for (int i = 0; i < weathers.size(); i++) {
            sum += weathers.get(i).getWindSpeed();
        }
        return sum / weathers.size();
    }

    private Double getAverageHumidity(ArrayList<Weather> weathers) {
        Double sum = 0d;
        for (int i = 0; i < weathers.size(); i++) {
            sum += weathers.get(i).getRelativeHumidity();
        }
        return sum / weathers.size();
    }

    private Double getAverageTemperature(ArrayList<Weather> weathers) {
        Double sum = 0d;
        for (int i = 0; i < weathers.size(); i++) {
            sum += weathers.get(i).getTemperature();
        }
        return sum / weathers.size();
    }

    private String getStrongestWind(ArrayList<Weather> weathers) {
        Double max = Double.MIN_VALUE;
        int index = 0;
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getWindSpeed() > max){
                max = weathers.get(i).getWindSpeed();
                index = i;
            }
        }
        return weathers.get(index).getDay() + "." + weathers.get(index).getMonth() + "." +
                weathers.get(index).getYear() + " в " + weathers.get(index).getTime() + " (" + max + " км/ч)";
    }

    private String getLowestHumidity(ArrayList<Weather> weathers) {
        Double min = Double.MAX_VALUE;
        int index = 0;
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getRelativeHumidity() < min){
                min = weathers.get(i).getRelativeHumidity();
                index = i;
            }
        }
        return weathers.get(index).getDay() + "." + weathers.get(index).getMonth() + "." +
                weathers.get(index).getYear() + " в " + weathers.get(index).getTime() + " (" + min + "%)";
    }

    private String getHighestTemperature(ArrayList<Weather> weathers) {
        Double max = Double.MIN_VALUE;
        int index = 0;
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getTemperature() > max){
                max = weathers.get(i).getTemperature();
                index = i;
            }
        }
        return weathers.get(index).getDay() + "." + weathers.get(index).getMonth() + "." +
                weathers.get(index).getYear() + " в " + weathers.get(index).getTime() + " (" + max + "°C)";
    }
}
