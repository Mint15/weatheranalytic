public class Weather {

    Integer year;
    String month;
    String day;
    String time;
    Double temperature;
    Double relativeHumidity;
    Double windSpeed;
    Double windDirection;

    public Weather(Integer year, String month, String day, String time, Double temperature,
                   Double relativeHumidity, Double windSpeed, Double windDirection) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.time = time;
        this.temperature = temperature;
        this.relativeHumidity = relativeHumidity;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
    }

    public Integer getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public String getDay() {
        return day;
    }

    public String getTime() {
        return time;
    }

    public Double getTemperature() {
        return temperature;
    }

    public Double getRelativeHumidity() {
        return relativeHumidity;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public Double getWindDirection() {
        return windDirection;
    }
}
